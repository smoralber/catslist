# CatsList

Demo application to learn how to use Android Architecture Components like Viewmodel and LiveData and another libraries that helps to boost Android application development.

## Structure

This project is separated in two different modules:

* `app` - Main app.
* `core` - Base definition for many components of the application (BaseActivity, BaseFragment, UseCase, etc).

## Architecture

This project is based on Clean Architecture and some concepts of MVVM.

## Dependencies

* [Kotlin](https://kotlinlang.org/)
* [Viewmodel](https://developer.android.com/reference/androidx/lifecycle/ViewModel).
* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata).
* [Coroutines](https://developer.android.com/kotlin/coroutines).
* [Retrofit](https://square.github.io/retrofit/).
* [koin](https://github.com/InsertKoinIO/koin).
* [okhttp](https://square.github.io/okhttp/).
* [Glide](https://github.com/bumptech/glide) Coil in mind for future.
* [Gson](https://github.com/google/gson).
* [RecyclerView](https://developer.android.com/jetpack/androidx/releases/recyclerview).

## Future work 

* Coil instead of Glide.
* Use Jetpack Compose for UI composing.
* Add ROOM to manage local storage.
* Make Core as independent library that will be used in many different projects.

This [trello](https://trello.com/b/krsqV49q/catslist) board is where I am working on.



