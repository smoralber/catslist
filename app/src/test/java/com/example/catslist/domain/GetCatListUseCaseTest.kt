package com.example.catslist.domain

import com.example.catslist.BaseUnitTest
import com.example.core.domain.Result
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class GetCatListUseCaseTest : BaseUnitTest() {

    @Mock
    private lateinit var repository: CatsRepository

    private lateinit var useCase: GetCatListUseCase

    @Before
    override fun setUp() {
        super.setUp()
        useCase = GetCatListUseCaseImpl(repository)
    }

    @Test
    fun `useCase should call repository and return list`() {
        runBlockingTest {
            whenever(repository.getCatList()).thenReturn(Result.Success(emptyList()))

            useCase.invoke(mock(), Unit)
            verify(repository).getCatList()
        }
    }
}