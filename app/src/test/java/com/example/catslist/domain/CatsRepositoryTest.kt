package com.example.catslist.domain

import com.example.catslist.BaseUnitTest
import com.example.catslist.data.api.CatsRemoteSource
import com.example.catslist.data.model.Cat
import com.example.catslist.data.repository.CatsRepositoryImpl
import com.example.core.domain.Failure
import com.example.core.domain.Result
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class CatsRepositoryTest : BaseUnitTest() {

    @Mock
    private lateinit var remoteSource: CatsRemoteSource

    private lateinit var repository: CatsRepository

    @Before
    override fun setUp() {
        super.setUp()
        repository = CatsRepositoryImpl(remoteSource)
    }

    @Test
    fun `repository should successfully call remote source to retrieve cats list`() {
        runBlockingTest {
            whenever(remoteSource.getCatList()).thenReturn(Result.Success(emptyList()))

            val result = repository.getCatList()

            assertEquals((result as Result.Success).success, listOf<Cat>())
        }
    }

    @Test
    fun `repository should call unsuccessfully remote source to retrieve cats list`() {
        runBlockingTest {
            whenever(remoteSource.getCatList()).thenReturn(Result.Error(Failure.RemoteError))

            val result = repository.getCatList()

            assertEquals((result as Result.Error).error, Failure.RemoteError)
        }
    }
}