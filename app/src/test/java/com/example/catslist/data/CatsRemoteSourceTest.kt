package com.example.catslist.data

import com.example.catslist.BaseUnitTest
import com.example.catslist.data.api.CatsApi
import com.example.catslist.data.api.CatsRemoteSource
import com.example.catslist.data.api.CatsRemoteSourceImpl
import com.example.catslist.data.model.Cat
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import retrofit2.Response

@ExperimentalCoroutinesApi
class CatsRepositoryTest : BaseUnitTest() {

    @Mock
    private lateinit var api: CatsApi

    private lateinit var remoteSource: CatsRemoteSource

    @Before
    override fun setUp() {
        super.setUp()
        remoteSource = CatsRemoteSourceImpl(api)
    }

    @Test
    fun `repository should call remote source successfully to retrieve cats list`() {
        runBlockingTest {
            whenever(api.getCatList(3)).thenReturn(Response.success(emptyList()))

            val result = api.getCatList(3)

            assertTrue(result.isSuccessful)
            assertEquals(result.body(), listOf<Cat>())
        }
    }

    @Test
    fun `repository should call remote source unsuccessfully to retrieve error`() {
        runBlockingTest {
            whenever(api.getCatList(3)).thenReturn(Response.error(500, mock()))

            val result = api.getCatList(3)

            assertFalse(result.isSuccessful)
            assertNotSame(result.body(), listOf<Cat>())
            assertEquals(result.code(), 500)
        }
    }
}