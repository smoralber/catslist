package com.example.catslist.view

import com.example.catslist.BaseUnitTest
import com.example.catslist.domain.GetCatListUseCase
import com.example.catslist.view.catList.CatsListViewModel
import com.example.core.domain.Result
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock


@ExperimentalCoroutinesApi
class CatsListViewModelTest : BaseUnitTest() {

    @Mock
    private lateinit var getCasListUseCase: GetCatListUseCase

    private lateinit var viewModelTest: CatsListViewModel


    @Before
    override fun setUp() {
        super.setUp()
        viewModelTest = CatsListViewModel(getCasListUseCase)
    }

    @Test
    fun `calling load cats method should return result successfully`() {
        runBlockingTest {
            whenever(getCasListUseCase.invoke(any(), any())).thenReturn(Result.Success(listOf()))

            viewModelTest.loadCats()

            assertEquals(viewModelTest.catsList.value, listOf<String>())
        }
    }
}