package com.example.catslist

import android.app.Application
import com.example.catslist.data.di.appDataModules
import com.example.catslist.domain.di.appModules
import com.example.catslist.data.di.networkModule
import com.example.catslist.view.di.appPresentationModule
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class CatsListApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            modules(listOf(appModules,
                networkModule, appDataModules, appPresentationModule))
        }
    }
}