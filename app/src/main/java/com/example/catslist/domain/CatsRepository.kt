package com.example.catslist.domain

import com.example.catslist.data.model.Cat
import com.example.core.domain.Failure
import com.example.core.domain.Result

interface CatsRepository {
    suspend fun getCatList(): Result<Failure, List<Cat>>
}