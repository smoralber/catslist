package com.example.catslist.domain.di

import com.example.catslist.domain.GetCatListUseCaseImpl
import org.koin.dsl.module


val appModules = module {

    factory { GetCatListUseCaseImpl(catsRepository = get()) }

}