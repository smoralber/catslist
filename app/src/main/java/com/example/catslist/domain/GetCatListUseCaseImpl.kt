package com.example.catslist.domain

import kotlin.coroutines.CoroutineContext

class GetCatListUseCaseImpl(private val catsRepository: CatsRepository) : GetCatListUseCase {

    override suspend fun invoke(context: CoroutineContext, params: Unit) =
        catsRepository.getCatList()
}