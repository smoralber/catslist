package com.example.catslist.domain

import com.example.catslist.data.model.Cat
import com.example.core.domain.UseCase

interface GetCatListUseCase : UseCase<Unit, List<Cat>>