package com.example.catslist.data.di

import com.example.catslist.data.api.CatsRemoteSourceImpl
import com.example.catslist.data.repository.CatsRepositoryImpl
import com.example.catslist.domain.CatsRepository
import org.koin.dsl.module

const val CAT_API_BASE_URL = "https://api.thecatapi.com/v1/"

val appDataModules = module {

    single<CatsRepository> { CatsRepositoryImpl(catsListRemoteSource = get()) }

    single { CatsRemoteSourceImpl(catsApi = get()) }
}