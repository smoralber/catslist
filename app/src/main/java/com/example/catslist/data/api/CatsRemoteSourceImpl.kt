@file:JvmName("CatsRemoteSourceKt")

package com.example.catslist.data.api

import com.example.catslist.data.model.Cat
import com.example.core.domain.Failure
import com.example.core.domain.Result

private const val NUMBER_OF_CATS = 30

open class CatsRemoteSourceImpl(private val catsApi: CatsApi) : CatsRemoteSource {

    override suspend fun getCatList(): Result<Failure, List<Cat>> =
        request(
            { catsApi.getCatList(NUMBER_OF_CATS) },
            { response -> response })
}