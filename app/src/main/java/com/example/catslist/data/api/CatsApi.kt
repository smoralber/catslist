package com.example.catslist.data.api

import com.example.catslist.data.model.Cat
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface CatsApi {

    @GET("images/search")
    suspend fun getCatList(@Query("limit") limit: Int): Response<List<Cat>>
}