package com.example.catslist.data.api

import com.example.catslist.data.model.Cat
import com.example.core.data.RemoteMapperSource
import com.example.core.domain.Failure
import com.example.core.domain.Result

interface CatsRemoteSource: RemoteMapperSource {

    suspend fun getCatList(): Result<Failure, List<Cat>>
}