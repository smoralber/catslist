package com.example.catslist.data.repository

import com.example.catslist.data.api.CatsRemoteSource
import com.example.catslist.data.model.Cat
import com.example.catslist.domain.CatsRepository
import com.example.core.domain.Failure
import com.example.core.domain.Result

class CatsRepositoryImpl(private val catsListRemoteSource: CatsRemoteSource) :
    CatsRepository {

    override suspend fun getCatList(): Result<Failure, List<Cat>>
            = catsListRemoteSource.getCatList()
}