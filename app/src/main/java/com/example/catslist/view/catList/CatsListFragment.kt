package com.example.catslist.view.catList

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.catslist.R
import com.example.catslist.view.NUMBER_OF_COLUMN
import com.example.catslist.view.catList.adapter.CatAdapter
import com.example.core.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CatsListFragment : BaseFragment(
    R.layout.fragment_main
), ItemClickListener {

    private val catsViewModel: CatsListViewModel by viewModel()
    private lateinit var catAdapter: CatAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        catAdapter = CatAdapter(this)

        catsRecyclerView.apply {
            if (catsViewModel.listState != null) {
                layoutManager?.onRestoreInstanceState(catsViewModel.listState)
                catsViewModel.listState = null
            } else {
                layoutManager = GridLayoutManager(activity, NUMBER_OF_COLUMN)
                adapter = catAdapter
                initViewModel()
            }
        }
    }

    private fun initViewModel() {
        catsViewModel.loadCats()

        catsViewModel.catsList.observe(viewLifecycleOwner, Observer { newCatsList ->
            catAdapter.updateData(newCatsList)
        })

        catsViewModel.showLoading.observe(viewLifecycleOwner, Observer { showLoading ->
            mainProgressBar?.visibility = if (showLoading) View.VISIBLE else View.GONE
        })

        catsViewModel.messageData.observe(viewLifecycleOwner, Observer {
            Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
        })
    }

    override fun itemClickListener(url: String) {
        val action = CatsListFragmentDirections.toDetailFragment(url)
        findNavController().navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        catsViewModel.listState = catsRecyclerView.layoutManager?.onSaveInstanceState()
    }
}

// Will be replaced by DataBinding when implemented
interface ItemClickListener {
    fun itemClickListener(url: String)
}