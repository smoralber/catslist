package com.example.catslist.view.splash

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy.ALL
import com.bumptech.glide.load.engine.DiskCacheStrategy.NONE
import com.bumptech.glide.request.RequestOptions.diskCacheStrategyOf
import com.example.catslist.R
import com.example.catslist.view.MainActivity
import com.example.core.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_splash_screen.*

private const val GIF_LOADER_URL = "https://media.giphy.com/media/EUNEHOZhspZRu/giphy.gif"

class SplashScreenFragment : BaseFragment(
    R.layout.fragment_splash_screen
) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {
            Glide.with(it)
                .load(GIF_LOADER_URL)
                .diskCacheStrategy(ALL)
                .into(ivCatLoading)
        }

        // Just to add a splash screen sample view
        val timer = object : CountDownTimer(1000, 1000) {
            override fun onFinish() {
                with(Intent(context, MainActivity::class.java)) {
                    startActivity(this)
                }.also {
                    activity?.finish()
                }
            }

            override fun onTick(millisUntilFinished: Long) {
                //Nothing to do
            }
        }
        timer.start()
    }
}