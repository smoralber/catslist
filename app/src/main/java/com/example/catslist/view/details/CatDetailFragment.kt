package com.example.catslist.view.details

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.catslist.R
import com.example.core.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_detail.*

class CatDetailFragment : BaseFragment(
    R.layout.fragment_detail
) {

    private val args: CatDetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {
            Glide.with(it)
                .load(args.catPhoto)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivCatDetail)
        }

    }
}