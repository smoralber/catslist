package com.example.catslist.view.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.catslist.R

class SplashActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

}