package com.example.catslist.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.catslist.R

const val NUMBER_OF_COLUMN = 3

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
