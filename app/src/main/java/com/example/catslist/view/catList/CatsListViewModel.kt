package com.example.catslist.view.catList

import androidx.lifecycle.MutableLiveData
import com.example.catslist.data.model.Cat
import com.example.catslist.domain.GetCatListUseCase
import com.example.core.domain.Failure
import com.example.core.view.BaseViewModel
import com.example.core.view.execute

class CatsListViewModel(private val getCatsListUseCase: GetCatListUseCase) : BaseViewModel() {

    val showLoading = MutableLiveData<Boolean>()
    val catsList = MutableLiveData<List<Cat>>()
    val messageData = MutableLiveData<String>()

    fun loadCats() {
        showLoading.postValue(true)
        execute {
            getCatsListUseCase(params = Unit).fold(
                ::handleError,
                ::handleSuccess
            )
        }
    }

    private fun handleSuccess(list: List<Cat>) {
        showLoading.postValue(false)
        catsList.postValue(list)
    }

    private fun handleError(failure: Failure) {
        showLoading.postValue(false)
        messageData.postValue("Error")
    }
}