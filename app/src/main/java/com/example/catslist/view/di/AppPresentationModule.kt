package com.example.catslist.view.di

import com.example.catslist.view.catList.CatsListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appPresentationModule = module {

    viewModel {
        CatsListViewModel(get())
    }
}