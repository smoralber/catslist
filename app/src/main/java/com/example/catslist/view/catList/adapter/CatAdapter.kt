package com.example.catslist.view.catList.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.catslist.R
import com.example.catslist.data.model.Cat
import com.example.catslist.view.catList.ItemClickListener
import kotlinx.android.synthetic.main.item_cat.view.*
import kotlin.properties.Delegates

class CatAdapter(
    private val itemClickListener: ItemClickListener
) : RecyclerView.Adapter<CatAdapter.CatViewHolder>() {

    private var catsList: List<Cat> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_cat, parent, false)
        return CatViewHolder(view)
    }

    override fun getItemCount(): Int = catsList.size

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        if (position != RecyclerView.NO_POSITION) {
            holder.bind(catsList[position])
            //Replaced when DataBinding implemented
            holder.itemView.setOnClickListener {
                itemClickListener.itemClickListener(catsList[position].imageUrl)
            }
        }
    }

    fun updateData(newCatsList: List<Cat>) {
        catsList = newCatsList
    }

    class CatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(cat: Cat) {
            Glide.with(itemView.context)
                .load(cat.imageUrl)
                .centerCrop()
                .placeholder(R.drawable.loader_placeholder)
                .thumbnail()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(itemView.itemCatImageView)
        }
    }
}