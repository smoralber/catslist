package com.example.core.data

import com.example.core.domain.Failure
import com.example.core.domain.Result
import retrofit2.Response

interface RemoteMapperSource {

    suspend fun <E,M> request (call: suspend () -> Response<E>, handleSuccess: (E) -> M, handleError: (Int) -> Failure = { Failure.RemoteError }): Result<Failure, M> =
        try {
            val response = call()
            val responseBody: E = response.body()!!
            if (response.code() == 200 || response.code() == 204) {
                Result.Success(handleSuccess(responseBody))
            } else {
                Result.Error(handleError(response.code()))
            }
        } catch (exception: Exception) {
            Result.Error(Failure.RemoteError)
        }
    }