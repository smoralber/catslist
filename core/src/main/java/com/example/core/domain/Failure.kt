package com.example.core.domain

sealed class Failure {
    object RemoteError: Failure()
}