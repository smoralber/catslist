package com.example.core.domain

sealed class Result<out E, out S> {
    data class Success<out S>(val success: S) : Result<Nothing,S>()
    data class Error<out E>(val error: E) : Result<E, Nothing>()

    fun fold(
        functionError: (E) -> Any,
        functionSuccess: (S) -> Any
    ): Any =
        when (this) {
            is Error -> functionError(error)
            is Success -> functionSuccess(success)
        }
}