package com.example.core.domain

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

interface UseCase<in Params, out Type> where Type : Any {

    suspend operator fun invoke( context: CoroutineContext = Dispatchers.IO, params: Params)
            : Result<Failure, Type> = withContext(context) { invoke(context, params) }
}