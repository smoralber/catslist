package com.example.core.view

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

fun BaseViewModel.execute(block: suspend CoroutineScope.() -> Unit) = launch { block(this)}