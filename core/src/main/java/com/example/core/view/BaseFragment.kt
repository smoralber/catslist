package com.example.core.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.fragment.app.Fragment

abstract class BaseFragment(
    private val layoutID: Int
) : Fragment() {

    private val onBackPressedAction: (OnBackPressedCallback.() -> Unit)? = null
    private lateinit var onBackPressedCallback: OnBackPressedCallback

    private var fragmentView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (fragmentView == null)
            fragmentView = inflater.inflate(layoutID, container, false)
        return fragmentView
    }

    override fun onStart() {
        super.onStart()
        // For custom back button action
        onBackPressedAction?.let {
            onBackPressedCallback =
                requireActivity().onBackPressedDispatcher.addCallback(onBackPressed = it)
        }
    }

    override fun onStop() {
        super.onStop()
        if (onBackPressedAction != null) {
            onBackPressedCallback.remove()
        }
    }
}